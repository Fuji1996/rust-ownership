fn move_operation(){
    // 1. Move operation
    println!("1. Move operation");
    // pass by value with basic data type
    let x: i32 = 5;
    let y = x;
    println!("value of x : {:?}", x);
    println!("value of y : {:?}", y);

    
    // move for heap allocated memory
    let x: String = String::from("hello ");
    let y = x; // moved the ownership of the value to y from x
    // same for all heap allocated memory, so x no longer exist
    // println!("value of x : {:?}", x);
    println!("value of y : {:?}", y);
    println!();
}

fn immutable_borrow(){
    // 2. Immutable borrow
    println!("2. Immutable borrow");
    let x: String = String::from("hello ");
    let y = &x; // here, '&' represnts the reference to the value, 
    // y borrows the value from x, and still x is the owner of the value
    println!("value of x : {:?}", x);
    println!("value of y : {:?}", y);
    println!();
}

fn mutable_borrow(){
    // 3. Mutable borrow
    println!("3. Mutable borrow");
    let mut x: String = String::from("hello ");
    let y = &mut x; // mutable borrow passes/moves ownership 
    // y is new owner and x no longer exist
    y.push_str(" there");
    //println!("value of x : {:?}", x);
    println!("value of y : {:?}", y);
    println!();
}

fn value_auto_deallocation(){
    // 4. Value auto deallocation
    println!("4. Value is deallocated");
    {
        let mut x: String = String::from("hello ");
        // value is getting deallocated automatically here when going out of scope
    }
    // println!("value of x : {:?}", x); // 
    println!();
}

fn reference_doesnt_outlive_value(){
    // 5. Reference doesn't outlive value
    println!("5. Reference doesn't outlive value");
    let y;
    {
           let mut x: String = String::from("hello ");
           y = &x; // y is borrowing the value of x and x is still the owner
    }
    // println!("value of y : {:?}", y); 
    
    // in the above example if we used 'y = x' instead of 'y = &x' that would mean transferring 
    // the ownership of the value from x to y and we could print the value later using y

    println!();
}

fn main() {
    /* 
    Here, we will look into some core concept of Rust memory management by simple examples : 
    => How Rust use unique 'ownership' concept in memory management
    => How this concept is different from C pointers
    => How this ensures memory safety along with some advantages what C pointers can't  
    */

    // 1. Move operation
    move_operation();

    // 2. Immutable borrow
    immutable_borrow();

    // 3. Mutable borrow
    mutable_borrow();

    // 4. Value auto deallocation
    value_auto_deallocation();

    // 5. Reference doesn't outlive value
    reference_doesnt_outlive_value();
}
